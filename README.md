# HMDS Optimal Sprite Manip Brute Forcer

A Lua script to try to find an optimal date, time and frame count for a hidden water sprite manip.

When this script is run, it attempts to find a manip that will result in the three hidden watering sprites
being located in the nearest possible three squares.
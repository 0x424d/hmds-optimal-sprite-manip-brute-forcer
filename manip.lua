gd = require("gd")

-- ------------------------------------------------------------------------------------------------------
-- Constants - edit when necessary
-- ------------------------------------------------------------------------------------------------------
-- Enable verbose / debug output
verbose = true
debug = false

local extraFrames = 0

-- Input delays prior to selecting "New game"
local framesToOpeningSequence = 354
local framesToNewGame = 439

-- Coords of green arrow, which indicates the game is waiting for input to move to the next text box
local arrowXCoord = 244
local arrowYCoord = 50

-- RGB values of green arrow
local arrowR = 140
local arrowG = 255
local arrowB = 189

-- Expected input delays when creating your farm after selecting "New game"
local playerNameInputDelay = 665
local playerBirthdayInputDelay = 833
local farmNameInputDelay = 1020
local dogNameInputDelay = 1191
local catNameInputDelay = 1364

-- Coords of "I" and "OK" tiles in naming screens
local iTileXCoord = 100
local iTileYCoord = 64
local okTileXCoord = 16
local okTileYCoord = 180

-- Coords of "Spring" and "1" tiles in birthday input screen
local springTileXCoord = 80
local springTileYCoord = 50
local firstTileXCoord = 50 -- To be clear, these are the coordinates of the "1" tile in the birthday input screen
local firstTileYCoord = 80

-- Input delay between the end of the opening cinematic and gaining control of Jack
local framesToGainControl = 18268

-- x / y coordinates of exit to Jack's house
local houseExitX = 104
local houseExitY = 217

-- Number of frames it takes to get control back after annoyed mayor
local framesToGainControlAfterMayor = 19110

-- Number of frames it takes for tool-switching animations to end
local switchAnimation = 17

-- x coordinate of watering hole
local wateringHoleX = 231

-- x / y coordinates of farm exit, and nearest farmland 
local farmExitX = 569
local farmExitY = 617
local villageLoadFrames = 14
local farmlandY = 129
local farmlandX = 1185

-- Number of frames in pickup animation
local pickupFrames = 16

-- Number of frames in "put item in bag" animation
local pocketFrames = 31

-- Number of frames it takes to use the hoe
local framesToUseHoe = 30

-- Number of frames it takes to use the watering can (when not filling it up)
local framesToUseWateringCan = 55

-- Number of frames it takes for a the spriteNumberOffset to start changing, if a sprite is unlocked
local framesToVariableChange = 237

-- Number of frames it takes for control to be returned to the player after a sprite is found
local framesToUnlockSprite = 200

-- Memory address of the number of sprites the player has unlocked
local spriteNumberOffset = 0x022C4D6D

-- Memory addresses of Jack's coordinates
local jackXOffset = 0x023D7AD0
local jackYOffset = 0x023D7AD2

-- Map number of sprites unlocked to values in above address
local mapSpriteValueToNo = {[110] = 1, [32] = 2, [210] = 3, [132] = 4, [54] = 5, [232] = 6}

-- Number of frames it takes to run from one square to the next
local framesToChangeSquare = 8

-- -----------------------------------------------------------------------------------------------------------
-- Functions
-- -----------------------------------------------------------------------------------------------------------
function advanceThroughTextBoxes(amount)
	for i = 1, amount do
		if verbose then print("Textbox", i, "/", amount) end
		while not greenArrowExists() do -- while text is being rendered
			-- hold L each frame to speed up text rendering
			joypad.set({L=true})
			emu.frameadvance()
		end
		-- text is done rendering for this text box, press A to move on to the next one
		if verbose then print("Tapping A") end
		joypad.set({L=true, A=true})
		emu.frameadvance()
		while greenArrowExists() do -- the arrow stays on screen for a few frames after you press A
			emu.frameadvance()
		end
	end
end

function greenArrowExists()
	r, g, b, palette = gui.getpixel(arrowXCoord, arrowYCoord)
	if r == arrowR and g == arrowG and b == arrowB and palette == arrowPalette then
		if verbose then print("\tFound arrow on frame", emu.framecount()) end
		return true
	else
		return false
	end
end

-- wait N frames - for actions that occur more than once
function waitNFrames(n)
	if verbose then print("Waiting", n + extraFrames, "frames") end
	i = 0
	for i = 1, n + extraFrames do
		emu.frameadvance()
	end
end

-- wait until a specific frame number
function waitUntilFrame(n, delayFrames)
	if verbose then print("Waiting until frame", n + delayFrames + extraFrames) end
	while emu.framecount() < n + delayFrames + extraFrames do
		emu.frameadvance()
	end
end

-- wait until input is possible (there's a delay between text boxes and input in the intro sequence)
function waitUntilInput()
	r, g, b, a = 0
	while r ~= 255 and g ~= 247 and b ~= 115 do
		if debug then print("Input unavailable") end
		emu.frameadvance()
		r, g, b, a = gui.getpixel(63, 127) -- the specific coords don't matter as long as it's yellow ONLY when input is accepted
	end
	if debug then print("Input available") end
end

function inputName()
	-- tap "I" tile, then "OK" tile, then select "yes"
	stylus.set({x=iTileXCoord, y=iTileYCoord, touch=true})
	emu.frameadvance()
	if debug then print("Tapped i") end
	stylus.set({x=okTileXCoord, y=okTileYCoord, touch=true})
	emu.frameadvance()
	if debug then print("Tapped OK") end
	emu.frameadvance()
	emu.frameadvance()
	joypad.set({up=true})
	emu.frameadvance()
	joypad.set({A=true})
	emu.frameadvance()
	if debug then print("Finished inputting name") end
end

function checkSquare(delayFrames)
	-- remove weeds / sticks / stones (hopefully no stumps / boulders - can't do much about them)
	if verbose then print("Removing debris") end
	joypad.set({A=true})
	emu.frameadvance()
	waitNFrames(pickupFrames)
	joypad.set({Y=true})
	emu.frameadvance()
	waitNFrames(framesToUseWateringCan)
	
	-- switch to and use hoe
	if verbose then print("Switching to hoe") end
	for i = 1, 2 do
		joypad.set({R=true})
		emu.frameadvance()
		joypad.set({R=true, B=true})
		emu.frameadvance()
		joypad.set({R=true})
		emu.frameadvance()
		waitNFrames(switchAnimation)
	end

	if verbose then print("Using hoe") end
	waitNFrames(2)
	joypad.set({Y=true})
	emu.frameadvance()
	waitNFrames(framesToUseHoe)
	
	-- switch to and use watering can
	if verbose then print("Switching to watering can") end
	for i = 1, 2 do
		joypad.set({R=true})
		emu.frameadvance()
		joypad.set({R=true, Y=true})
		emu.frameadvance()
		joypad.set({R=true, Y=false})
		emu.frameadvance()
		waitNFrames(switchAnimation)
	end

	if verbose then print("Using watering can") end
	waitNFrames(2)
	joypad.set({Y=true})
	emu.frameadvance()
	waitNFrames(framesToUseWateringCan)
	
	-- check if sprite found
	-- if you have unlocked a sprite, there is always a lag frame 2 frames after you unlock it
	waitNFrames(2)
	if emu.lagged() then
		print("!!!Found a sprite!!!")
		advanceThroughTextBoxes(3)
		waitNFrames(framesToUnlockSprite)
		spriteValue = memory.readbyte(spriteNumberOffset)
		if verbose then print(string.format("%x: %d (%d sprites unlocked!)", spriteNumberOffset, spriteValue, mapSpriteValueToNo[spriteValue])) end
		moveNFrames("right", framesToChangeSquare)
		return true
	else
		moveNFrames("right", framesToChangeSquare)
		return false
	end
end

-- move until reaching a given x coordinate
function moveX(x)
	while memory.readword(jackXOffset) ~= x do
		if debug then print("X coord:", memory.readword(jackXOffset)) end
		
		if emu.framecount() > 21000 then -- if we get stuck somehow
			return
		elseif memory.readword(jackXOffset) > x then
			joypad.set({B=true, left=true})
		else
			joypad.set({B=true, right=true})
		end
		emu.frameadvance()
	end
end

-- move until reaching a given y coordinate
function moveY(y)
	while memory.readword(jackYOffset) ~= y do
		if debug then print("Y coord:", memory.readword(jackYOffset)) end
		
		if emu.framecount() > 21000 then
			return
		elseif memory.readword(jackYOffset) > y then
			joypad.set({B=true, up=true})
		else
			joypad.set({B=true, down=true})
		end
		emu.frameadvance()
	end
end

-- move in a given direction for n frames
function moveNFrames(direction, n)
	if verbose then print("Moving", n, "frames in", direction, "direction") end
	joytable = {}
	joytable["B"] = true
	joytable[direction] = true
	for i = 1, n do
		joypad.set(joytable)
		emu.frameadvance()
	end
end

function incrementSysclock(oldDatetime)
	datetimeInt = os.time(oldDatetime)
	minute = (os.date("%M", datetimeInt) + 1) % 60
	if minute == 0 then -- an hour has passed
		hour = (os.date("%H", datetimeInt) + 1) % 24
		if hour == 0 then -- a day has passed
			day = (os.date("%d", datetimeInt) + 1) % (daysInMonth(os.date("%m", datetimeInt), os.date("%y", datetimeInt)) + 1)
		end
		if day == 0 then -- a month has passed
			day = 1 -- can't have a 0th day
			month = (os.date("%m", datetimeInt) + 1) % 13
		end
		if month == 0 then -- a year has passed
			month = 1 -- or a 0th month
			year = (os.date("%Y", datetimeInt) + 1)
		end
	else
		hour = os.date("%H", datetimeInt)
		day = os.date("%d", datetimeInt)
		month = os.date("%m", datetimeInt)
		year = os.date("%Y", datetimeInt)
	end
	
	dateStr = string.format("%02d-%02d-%d", month, day, year)
	timeStr = string.format("%02d:%02d", hour, minute)
	
	os.execute("date " .. dateStr)
	os.execute("time " .. timeStr)
	
	return os.date("*t", os.time({min=minute, hour=hour, day=day, month=month, year=year}))
end

function daysInMonth(month, year)
	if month == 2 then -- February
		if year % 4 == 0 and year % 100 ~= 0 then -- if a leap year
			return 29
		else
			return 28
		end
	elseif month == 4 or month == 6 or month == 9 or month == 11 then -- 30-day months
		return 30
	else -- 31-day months
		return 31
	end
end

-- This was a pain to get working because the documentation for both desmume and luagd... sucks
-- Desmume doesn't have a regular gui.savescreenshot() function like FCEUX. It has to be done with gui.gdscreenshot()
-- which means you need to build luagd from source or install it indirectly
-- on windows it's easier to do the latter
-- If anyone else wants to get this working on windows in the future, here's what you need to do:
--     Install LuaForWindows somewhere (https://github.com/rjpcomputing/luaforwindows/releases)
--     Copy <LuaForWindows install directory>/lua51.dll to the folder you're running the script from
--     Copy <LuaForWindows install directory>/clibs/gd.dll to the folder you're running the script from
--     Copy <LuaForWindows install directory>/include to the folder you're running the script from
function takeScreenshot(x, y, date, frame)
	moveX(x)
	moveY(y)

	local filename = string.format("screenshots/%d-%02d-%02d_%02d;%02d_%d.png", date.year, date.month, date.day, date.hour, date.min, frame)
	
	gd.createFromGdStr(gui.gdscreenshot()):png(filename) -- github.com/TASVideos/desmume/blob/master/desmume/src/lua-engine.cpp -> Ctrl+F -> gui.gdscreenshot() - documentation for this crappy emulator is nonexistent
end
	

-- ---------------------------------------------------------------------------------------------------------------
-- Main loop
-- ---------------------------------------------------------------------------------------------------------------

function main(startDate, startFrame)
	if startDate == nil then
		oldDatetime = {year=2000, month=01, day=01, hour=00, min=00}
	else
		oldDatetime = startDate
	end
	os.execute("date 01-01-2000") -- minimum NDS clock date, dd-mm-yyyy
	os.execute("time 00:00") -- minimum NDS clock time

	local delayFrames = 0
	local startFromArg = true
	while oldDatetime.year < 2100 do -- until all possible dates have been tested - NDS clock has a maximum value of 12/31/2099 23:59
		print(string.format("oldDatetime: {year = %d, month = %d, day = %d, hour = %d, min = %d}", oldDatetime.year, oldDatetime.month, oldDatetime.day, oldDatetime.hour, oldDatetime.min))
		if startFrame == nil or startFromArg == false then
			if verbose then print("startFrame is nil") end
			delayFrames = 0
		else
			if verbose then print("startFrame is not nil") end
			if startFromArg then
				delayFrames = startFrame
			end
			startFromArg = false
		end
		while delayFrames <= 60 do -- not interested in waiting more than 1 second for this manip, if possible
			delayFrames = delayFrames + 1
			print("Set delayFrames to", delayFrames)			
			os.execute(string.format("date %d-%d-%d", oldDatetime.day, oldDatetime.month, oldDatetime.year))
			os.execute(string.format("time %d:%d", oldDatetime.hour, oldDatetime.min))

			local prettyDate = string.format("%d-%02d-%02d %02d:%02d", oldDatetime.year, oldDatetime.month, oldDatetime.day, oldDatetime.hour, oldDatetime.min)
			
			function onExit()
				print("============================")
				print("Exited on date", prettyDate, ", delay frame", delayFrames)
				print("============================")
			end			

			emu.registerexit(onExit)

			print("--------------------------")
			print(string.format("Testing date %s, with a delay of %d frames", prettyDate, delayFrames))
		
			print("Resetting emulator")
			emu.reset()
			print("Emulator reset")
		
			waitNFrames(framesToOpeningSequence)
			print("Tapping opening sequence on frame", emu.framecount())
			stylus.set({x=1, y=1, touch=true})
			emu.frameadvance()

			waitUntilFrame(framesToNewGame, delayFrames)
			print(string.format("Tapping new game on frame %d (expected %d + %d = %d)", emu.framecount(), framesToNewGame, delayFrames, framesToNewGame + delayFrames))
			stylus.set({x=1, y=1, touch=true})
			emu.frameadvance()
		
			-- text boxes 1-2
			advanceThroughTextBoxes(2)
			waitUntilInput()
			print(string.format("Begin input name on frame %d (expected %d + %d = %d)", emu.framecount(), playerNameInputDelay, delayFrames, playerNameInputDelay + delayFrames))
			inputName()
		
			-- text box 3
			advanceThroughTextBoxes(1)
			waitUntilInput()
			print(string.format("Begin birthday input on frame %d (expected %d + %d = %d)", emu.framecount(), playerBirthdayInputDelay, delayFrames, playerBirthdayInputDelay + delayFrames))

			-- special case; birthday input
			stylus.set({x=springTileXCoord, y=springTileYCoord, touch=true})
			emu.frameadvance()
			stylus.set({touch=false})
			emu.frameadvance()
			stylus.set({x=firstTileXCoord, y=firstTileYCoord, touch=true})
			emu.frameadvance()
			stylus.set({x=okTileXCoord, y=okTileYCoord, touch=true})
			emu.frameadvance()
			emu.frameadvance()
			stylus.set({touch=false})
			emu.frameadvance()
			joypad.set({up=true})
			emu.frameadvance()
			joypad.set({A=true})
			emu.frameadvance()
			
			-- text box 4
			advanceThroughTextBoxes(1)
			waitUntilInput()
			print(string.format("Begin farm name input on frame %d (expected %d + %d = %d)", emu.framecount(), farmNameInputDelay, delayFrames, farmNameInputDelay + delayFrames))
			inputName()
			
			-- text box 5
			advanceThroughTextBoxes(1)
			waitUntilInput()
			print(string.format("Begin dog name input on frame %d (expected %d + %d = %d)", emu.framecount(), dogNameInputDelay, delayFrames, dogNameInputDelay + delayFrames))
			inputName()
			
			-- text box 6
			advanceThroughTextBoxes(1)
			waitUntilInput()
			print(string.format("Begin cat name input on frame %d (expected %d + %d = %d)", emu.framecount(), catNameInputDelay, delayFrames, catNameInputDelay + delayFrames))
			inputName()
			
			-- text boxes 7-94 (opening cinematic)
			advanceThroughTextBoxes(89)
			
			-- leaving Jack's house
			--print(string.format("Gained control of Jack on frame %d (expected %d + %d = %d)", emu.framecount(), framesToGainControl, delayFrames, framesToGainControl + delayFrames))
			moveX(houseExitX)
			moveY(houseExitY)

			-- text boxes 95-97 (mayor) + select "I know. Go away."
			advanceThroughTextBoxes(3)
			waitNFrames(2)
			joypad.set({down=true})
			emu.frameadvance()
			joypad.set({A=true})
			emu.frameadvance()
			
			-- text boxes 98-99 (annoyed mayor)
			advanceThroughTextBoxes(2)
			--print(string.format("Gained control of Jack for the second time on frame %d (expected %d + %d = %d)", emu.framecount(), framesToGainControlAfterMayor, delayFrames, framesToGainControlAfterMayor + delayFrames))
			
			-- run to watering hole
			moveX(wateringHoleX)

			-- select watering can
			for i = 1, 2 do
				joypad.set({R=true})
				emu.frameadvance()
				joypad.set({R=true, B=true})
				emu.frameadvance()
				joypad.set({R=true})
				emu.frameadvance()
				waitNFrames(switchAnimation)
			end

			-- fill watering can
			waitNFrames(2)
			joypad.set({Y=true})
			emu.frameadvance()
			
			-- run to field nearest farm
			moveX(farmExitX)
			moveY(farmExitY)
			waitNFrames(villageLoadFrames)
			moveY(farmlandY)
			moveX(farmlandX)			
			
			-- hoe + water 3 squares, checking for sprites
			spriteExists = {false, false, false}
			for i = 1, 3 do
				spriteExists[i] = checkSquare()
			end
			
			if spriteExists[1] == true and spriteExists[2] == true and spriteExists[3] == true then
				print("Optimal manip found!")
				print("Date: " .. newDate .. ", frame: " .. delayFrames)
				return
			else
				print("Non-optimal manip. Taking screenshot, then resetting.")
			end
			takeScreenshot(1255, 187, oldDatetime, delayFrames)
		end
		oldDatetime = incrementSysclock(oldDatetime)
	end
	-- no optimal manip found
	print("Finished script. No optimal manips found.")
end

main({year=2000, month=01, day=01, hour=00, min=26}, 34)